
var proto = Animation.prototype

proto.start = function() {
	var self = this
	var start, end, perc

	self.isAnimating = true
	requestAnimationFrame( onFrame )

	function onFrame() {
		var time = Date.now().getTime()
		if( !start ) {
			start = time
			end = start + self.dur
		}
		perc =  (start - time) / dur
		if( perc > 1 ) perc = 1
		if( self.callback ) self.callback( perc )
		if( perc < 1 && self.isAnimating ) requestAnimationFrame( onFrame )
	}
}

proto.stop = function() {
	this.isAnimating = false
}

module.exports = Animation

function Animation( dur, callback ) {
	this.dur = dur || 1000
	this.startTime = 0
	this.callback = callback
}
