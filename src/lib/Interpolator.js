var proto = Interpolator.prototype

proto.get = function( perc ) {
	var p, cur = {}, start = this.start, rng = this.range
	for( p in start ) cur[ p ] = start[ p ] + range[ p ]
	return cur
}

module.exports = Interpolator

function Interpolator( start, end ) {

	var p, range = {}

	this.start = start
	this.end = end
	this.range = range

	for( p in start ) range[ p ] = end[ p ] - start[ p ]

}
