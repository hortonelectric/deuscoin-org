var top = $( '#top' )
var can = $( '<canvas>' )
var win = $(window)
var stockLines = require( './stockLines' )
var l1, l2, l3, world1, world2

$(init)

function init() {
	can.css({
		backgroundColor: 'rgba(0,0,0,0.25)',
		//transform: 'rotateY(45deg)'
	})

	top.append( can )

	paper.install( window )
	paper.setup( can[0] )

	l1 = new Layer()
	l2 = new Layer()
	l3 = new Layer()

	l1.activate()
	project.importSVG( 'anim/world.svg', {onLoad: function( svg ){
		world1 = svg
		world1.opacity = .3
		world2 = world1.clone()
		world1.fillColor = 'red'
		world2.fillColor = 'red'
		resize()
		stockLines.create( l3, l2)
		paper.view.onFrame = onFrame
		win.on( 'resize', resize )
		resize()
	}} )

}

function resize() {

	var w = win.width()
	var h = win.height()
	var ww, wh
	can.attr( { width: w, height: h } )
	can.css( { width: w, height: w } )
	can.css( { backgroundColor: 'transparent' } )
	top.attr( { width: w, height: w } )
	view.width = w
	view.height = h
	if( world1 ) {
		var ww = view.width / world1.bounds.width
		var wh = view.height / world1.bounds.height * .7
		world1.scaling = world2.scaling = ww > wh ? ww : wh
		world1.position = view.center
		world2.position = world1.position.clone()
		world2.position.x += world2.bounds.width
	}

}

function onFrame() {

	var w = win.width()
	var h = win.height()
	var sl, len = stockLines.length

	can.attr( { width: w, height: h } )
	can.css( { width: w, height: h } )
	view.size.width = w
	view.size.height = h

	stockLines.onFrame()

	if( world1 ) {

		world1.position.x -= .5
		world2.position.x -= .5
		world1.position.y = world2.position.y = view.center.y

		if( world1.position.x < view.center.x - world1.bounds.width ) {
			world1.position.x = view.center.x + world1.bounds.width
		}
		if( world2.position.x < view.center.x - world2.bounds.width ) {
			world2.position.x = view.center.x + world2.bounds.width
		}
	}

}
