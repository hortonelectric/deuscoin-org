var bars = Object.create( null )
var layer

bars.create = function( _layer ) {
	layer = _layer
	createBar( 0 )
}

bars.createBar = createBar

module.exports = bars

function createBar( x ) {
	var f = 0
	var path, h = view.height * 0.25 + rand() * view.height * 0.25
	layer.activate()
	path = new Path.Rectangle( x + 0, view.height, 50, 0 )
	path.fillColor = 'rgba( 250, 0, 0, 0.5 )'
	path.strokeColor = 'rgba( 255, 0, 0, .8 )'
	setTimeout( fadein, 0 )

	function fadein() {
		var s2 = path.segments[1]
		var s3 = path.segments[2]
		var s4 = path.segments[3]
		f += 0.05
		s3.point.y = s2.point.y = s4.point.y - f * h

		if( f < 1 ) setTimeout( fadein, 50 )
	}
}
