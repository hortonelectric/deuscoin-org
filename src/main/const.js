window.PI = Math.PI
window.PI2 = PI * 2
window.rand = Math.random
window.CONST = {
	newPoint: {
		rate: 5,
		x: {
			rnd: 0,
			con: 10
		},
		y: {
			rng: 10000,
			bal: 6200,
			pow: .3,
			sinDuration: 250,
			sinWeight: 5
		}
	},
	medianLength: 5,
	stockLine: {
		gradientStops: ["rgba( 255, 255, 255, .25 )", "rgba( 255, 255, 255, .75)", "rgba( 255, 255, 255, .5)", "rgba( 255, 0,0, 0.25)", "rgba( 0, 0, 0, 0)"],
		gradientStops2: ["rgba( 0, 0, 255, .9 )", "rgba( 0, 0, 255, .85)", "rgba( 0, 0, 255, .8)", "rgba( 255, 0,0, 0.25)", "rgba( 0, 0, 0, 0)"]
	}
}
