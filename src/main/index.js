require( './const' )
require( './sidenav' )

var floor = Math.floor
var win = $( window )
var bod = $( 'body' )
var top = $( '#top' )
var nav = $( 'nav.main' )

require( './anim' )

initNav()
win.on( 'scroll', onScroll )
onScroll()

function initNav() {
	var links = $( 'nav.main ul li a' )

	links.each(function(index, el) {

		el = $(el)
		var h = el.attr( 'href' )
		console.log( h )

		if( h[0] === '#' ) {
			var a = $( el.attr( 'href' ) )

			win.on( 'scroll', function() {
				var ot = a.offset().top - 120
				var st = win.scrollTop()
				if( st > ot ) {
					links.each( function(i, e) { $(e).removeClass( 'active' ) })
					el.addClass( 'active' )
				}
			})
		}
	})
}

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
		closeNav()
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 118
      }, 400, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

function onScroll() {
	var st = win.scrollTop()
	var navTgt = top.height() - 100
	var navHeight = nav.height() + 5
	var perc = st / navTgt
	if( perc > 1 ) perc = 1
	nav.css({ top: perc * navHeight - navHeight })
}
