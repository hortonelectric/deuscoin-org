var body = $( 'body' )

window.openNav = function openNav() {
    body.addClass( 'navOpen' )
}

window.closeNav = function closeNav() {
    body.removeClass( 'navOpen' )
}
