var stockLines = []
var np = CONST.newPoint
var curFrame = 0
var mcount = 0
var sl1, sl2, sl3, nextPoint
var nextMedian, nextMedian2, layer, barsLayer
var bars, shouldCreateBar

stockLines.create = function( _layer, _barsLayer ) {

	layer = _layer, barsLayer = _barsLayer
	bars = require( './bars' )
	bars.create( barsLayer )
	layer.activate()
	layer.addChild( barsLayer )
	sl1 = new StockLine( true )
	sl2 = new StockLine( false, CONST.stockLine.gradientStops2 )
	sl3 = new StockLine()

	sl2.circle.fillColor = 'rgba( 0, 100, 255, 1 )'
	sl2.path.strokeColor = 'rgba( 0, 100, 255, 1 )'

	sl3.circle.fillColor = 'rgba( 200, 0, 0, 1 )'
	sl3.path.strokeColor = 'rgba( 200, 0, 0, 1 )'

	createNewPoint()

}

stockLines.onFrame = function() {

	var mrate = (np.rate * CONST.medianLength)
	var mrate2 = mrate * CONST.medianLength
	var perc = (curFrame++ % np.rate) / np.rate
	var perc2 = curFrame % mrate / mrate
	var perc3 = curFrame % mrate2 / mrate2
	var diff = nextPoint.multiply( perc )

	var diff2, diff3

	if( perc === 0 ) createNewPoint()

	sl1.onFrame( diff )

	if( perc2 === 0 ) createNewMedian()
	if( perc3 === 0 ) createNewMedian2()

	if( nextMedian ) {
		diff2 = nextMedian.multiply( perc2 )
		sl2.onFrame( diff2 )
	}
	if( nextMedian2 ) {
		diff3 = nextMedian2.multiply( perc3 )
		sl3.onFrame( diff3 )
	}

	if( layer.bounds.width > view.width * 0.98 ) {
		layer.position.x = - (layer.bounds.width / 2 ) + view.width * .98
	}

	layer.position.y += .1 * (( view.height *0.4 + layer.bounds.height * .5 ) - layer.position.y )

}

module.exports = stockLines

function createNewPoint() {
	var clone
	sl1.ap = sl1.path.lastSegment.point
	nextPoint = newPoint()
	clone = sl1.ap.clone()
	sl1.path.add( clone )
}

function createNewMedian() {
	var ML = CONST.medianLength
	var i, x1 = sl1.path.segments.length - ML, x2 = x1 + ML
	var sum = 0
	var clone

	sl2.ap = sl2.path.lastSegment.point

	for( i = x1; i < x2; ++i ) {

		sum += sl1.path.segments[ i ].point.y
	}
	nextMedian = new Point(
		sl1.path.segments[ x1 ].point.x - sl2.ap.x + 20,
		sum / ML - sl2.ap.y
	)
	clone = sl2.ap.clone()
	sl2.path.add( clone )
	shouldCreateBar = !shouldCreateBar
	if( x2 > 10 && shouldCreateBar ) bars.createBar( clone.x )
}

function createNewMedian2() {
	var ML = CONST.medianLength
	var i, x1 = sl2.path.segments.length - ML, x2 = x1 + ML
	var sum = 0
	var clone

	sl3.ap = sl3.path.lastSegment.point

	for( i = x1; i < x2; ++i ) {

		sum += sl2.path.segments[ i ].point.y

	}

	nextMedian2 = new Point(
		sl2.path.segments[ x1 ].point.x - sl3.ap.x + 270,
		sum / ML - sl3.ap.y
	)
	clone = sl3.ap.clone()
	sl3.path.add( clone )

}

function StockLine( grad, stops ) {
	var offx = -25
	var path = this.path = new Path()
	var circle = this.circle = new Path.Circle( new Point( offx, view.center.y ), 4 )
	var br, bl, ap

	circle.fillColor = 'white'
	this.perc = 0
	stockLines.push( this )

	if( grad ) {
		br = path.add( new Point( offx, view.center.y) )
		bl = path.add( new Point( offx, view.center.y) )
		path.fillColor = {
	    gradient: {
	      stops: stops || CONST.stockLine.gradientStops
	    },
	    origin: new Point(offx, 0),
	    destination: new Point(offx, 100)
	  }
	}

	path.add( new Point( offx, view.center.y ) )

	path.strokeColor = 'rgba(255, 255, 255, .8 )'
	path.strokeWidth = 2

	this.onFrame = function( diff ) {

		var ls = path.lastSegment

		ls.point.x = this.ap.x + diff.x
		ls.point.y = this.ap.y + diff.y

		circle.fillColor.alpha =
			(Math.sin( Date.now() % 1000 / 1000 * PI2 ) + 1) * 0.1 + 0.4

		if( grad ) {
			bl.point.x = -3
			bl.point.y = view.height + 3
			br.point.x = ls.point.x
			br.point.y = view.height + 3
			path.fillColor.origin = path.bounds.topRight
			path.fillColor.destination = br.point
		}

		circle.position = ls.point

		if( grad ) {
			mp = path.segments[ 2 ]
			if( mp && mp.point.x < -1000 ) {
				path.removeSegment( 2 )
			}
		} else {
			mp = path.segments[ 0 ]
			if( mp && mp.point.x < -1000 ) {
				path.removeSegment( 0 )
			}
		}

	}

}

function newPoint() {

	var sinx, s

	x = rand() * np.x.rnd + np.x.con
	y = rand() * np.y.rng - np.y.bal
	x = 10


	if( y > 0 ) {
		y = Math.pow( y, np.y.pow )
	} else {
		y = Math.pow( Math.abs(y), np.y.pow ) * -1
	}

	sinx = curFrame % np.y.sinDuration / np.y.sinDuration

	s = Math.sin( sinx * PI2 ) * np.y.sinWeight
	y += s

	return new Point( x, y )

}
