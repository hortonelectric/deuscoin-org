(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var top = $( '#top' )
var can = $( '<canvas>' )
var win = $(window)
var stockLines = require( './stockLines' )
var l1, l2, l3, world1, world2

$(init)

function init() {
	can.css({
		backgroundColor: 'rgba(0,0,0,0.25)',
		//transform: 'rotateY(45deg)'
	})

	top.append( can )

	paper.install( window )
	paper.setup( can[0] )

	l1 = new Layer()
	l2 = new Layer()
	l3 = new Layer()

	l1.activate()
	project.importSVG( 'anim/world.svg', {onLoad: function( svg ){
		world1 = svg
		world1.opacity = .3
		world2 = world1.clone()
		world1.fillColor = 'red'
		world2.fillColor = 'red'
		resize()
		stockLines.create( l3, l2)
		paper.view.onFrame = onFrame
		win.on( 'resize', resize )
		resize()
	}} )

}

function resize() {

	var w = win.width()
	var h = win.height()
	var ww, wh
	can.attr( { width: w, height: h } )
	can.css( { width: w, height: w } )
	can.css( { backgroundColor: 'transparent' } )
	top.attr( { width: w, height: w } )
	view.width = w
	view.height = h
	if( world1 ) {
		var ww = view.width / world1.bounds.width
		var wh = view.height / world1.bounds.height * .7
		world1.scaling = world2.scaling = ww > wh ? ww : wh
		world1.position = view.center
		world2.position = world1.position.clone()
		world2.position.x += world2.bounds.width
	}

}

function onFrame() {

	var w = win.width()
	var h = win.height()
	var sl, len = stockLines.length

	can.attr( { width: w, height: h } )
	can.css( { width: w, height: h } )
	view.size.width = w
	view.size.height = h

	stockLines.onFrame()

	if( world1 ) {

		world1.position.x -= .5
		world2.position.x -= .5
		world1.position.y = world2.position.y = view.center.y

		if( world1.position.x < view.center.x - world1.bounds.width ) {
			world1.position.x = view.center.x + world1.bounds.width
		}
		if( world2.position.x < view.center.x - world2.bounds.width ) {
			world2.position.x = view.center.x + world2.bounds.width
		}
	}

}

},{"./stockLines":5}],2:[function(require,module,exports){
var bars = Object.create( null )
var layer

bars.create = function( _layer ) {
	layer = _layer
	createBar( 0 )
}

bars.createBar = createBar

module.exports = bars

function createBar( x ) {
	var f = 0
	var path, h = view.height * 0.25 + rand() * view.height * 0.25
	layer.activate()
	path = new Path.Rectangle( x + 0, view.height, 50, 0 )
	path.fillColor = 'rgba( 250, 0, 0, 0.5 )'
	path.strokeColor = 'rgba( 255, 0, 0, .8 )'
	setTimeout( fadein, 0 )

	function fadein() {
		var s2 = path.segments[1]
		var s3 = path.segments[2]
		var s4 = path.segments[3]
		f += 0.05
		s3.point.y = s2.point.y = s4.point.y - f * h

		if( f < 1 ) setTimeout( fadein, 50 )
	}
}

},{}],3:[function(require,module,exports){
window.PI = Math.PI
window.PI2 = PI * 2
window.rand = Math.random
window.CONST = {
	newPoint: {
		rate: 5,
		x: {
			rnd: 0,
			con: 10
		},
		y: {
			rng: 10000,
			bal: 6200,
			pow: .3,
			sinDuration: 250,
			sinWeight: 5
		}
	},
	medianLength: 5,
	stockLine: {
		gradientStops: ["rgba( 255, 255, 255, .25 )", "rgba( 255, 255, 255, .75)", "rgba( 255, 255, 255, .5)", "rgba( 255, 0,0, 0.25)", "rgba( 0, 0, 0, 0)"],
		gradientStops2: ["rgba( 0, 0, 255, .9 )", "rgba( 0, 0, 255, .85)", "rgba( 0, 0, 255, .8)", "rgba( 255, 0,0, 0.25)", "rgba( 0, 0, 0, 0)"]
	}
}

},{}],4:[function(require,module,exports){
var body = $( 'body' )

window.openNav = function openNav() {
    body.addClass( 'navOpen' )
}

window.closeNav = function closeNav() {
    body.removeClass( 'navOpen' )
}

},{}],5:[function(require,module,exports){
var stockLines = []
var np = CONST.newPoint
var curFrame = 0
var mcount = 0
var sl1, sl2, sl3, nextPoint
var nextMedian, nextMedian2, layer, barsLayer
var bars, shouldCreateBar

stockLines.create = function( _layer, _barsLayer ) {

	layer = _layer, barsLayer = _barsLayer
	bars = require( './bars' )
	bars.create( barsLayer )
	layer.activate()
	layer.addChild( barsLayer )
	sl1 = new StockLine( true )
	sl2 = new StockLine( false, CONST.stockLine.gradientStops2 )
	sl3 = new StockLine()

	sl2.circle.fillColor = 'rgba( 0, 100, 255, 1 )'
	sl2.path.strokeColor = 'rgba( 0, 100, 255, 1 )'

	sl3.circle.fillColor = 'rgba( 200, 0, 0, 1 )'
	sl3.path.strokeColor = 'rgba( 200, 0, 0, 1 )'

	createNewPoint()

}

stockLines.onFrame = function() {

	var mrate = (np.rate * CONST.medianLength)
	var mrate2 = mrate * CONST.medianLength
	var perc = (curFrame++ % np.rate) / np.rate
	var perc2 = curFrame % mrate / mrate
	var perc3 = curFrame % mrate2 / mrate2
	var diff = nextPoint.multiply( perc )

	var diff2, diff3

	if( perc === 0 ) createNewPoint()

	sl1.onFrame( diff )

	if( perc2 === 0 ) createNewMedian()
	if( perc3 === 0 ) createNewMedian2()

	if( nextMedian ) {
		diff2 = nextMedian.multiply( perc2 )
		sl2.onFrame( diff2 )
	}
	if( nextMedian2 ) {
		diff3 = nextMedian2.multiply( perc3 )
		sl3.onFrame( diff3 )
	}

	if( layer.bounds.width > view.width * 0.98 ) {
		layer.position.x = - (layer.bounds.width / 2 ) + view.width * .98
	}

	layer.position.y += .1 * (( view.height *0.4 + layer.bounds.height * .5 ) - layer.position.y )

}

module.exports = stockLines

function createNewPoint() {
	var clone
	sl1.ap = sl1.path.lastSegment.point
	nextPoint = newPoint()
	clone = sl1.ap.clone()
	sl1.path.add( clone )
}

function createNewMedian() {
	var ML = CONST.medianLength
	var i, x1 = sl1.path.segments.length - ML, x2 = x1 + ML
	var sum = 0
	var clone

	sl2.ap = sl2.path.lastSegment.point

	for( i = x1; i < x2; ++i ) {

		sum += sl1.path.segments[ i ].point.y
	}
	nextMedian = new Point(
		sl1.path.segments[ x1 ].point.x - sl2.ap.x + 20,
		sum / ML - sl2.ap.y
	)
	clone = sl2.ap.clone()
	sl2.path.add( clone )
	shouldCreateBar = !shouldCreateBar
	if( x2 > 10 && shouldCreateBar ) bars.createBar( clone.x )
}

function createNewMedian2() {
	var ML = CONST.medianLength
	var i, x1 = sl2.path.segments.length - ML, x2 = x1 + ML
	var sum = 0
	var clone

	sl3.ap = sl3.path.lastSegment.point

	for( i = x1; i < x2; ++i ) {

		sum += sl2.path.segments[ i ].point.y

	}

	nextMedian2 = new Point(
		sl2.path.segments[ x1 ].point.x - sl3.ap.x + 270,
		sum / ML - sl3.ap.y
	)
	clone = sl3.ap.clone()
	sl3.path.add( clone )

}

function StockLine( grad, stops ) {
	var offx = -25
	var path = this.path = new Path()
	var circle = this.circle = new Path.Circle( new Point( offx, view.center.y ), 4 )
	var br, bl, ap

	circle.fillColor = 'white'
	this.perc = 0
	stockLines.push( this )

	if( grad ) {
		br = path.add( new Point( offx, view.center.y) )
		bl = path.add( new Point( offx, view.center.y) )
		path.fillColor = {
	    gradient: {
	      stops: stops || CONST.stockLine.gradientStops
	    },
	    origin: new Point(offx, 0),
	    destination: new Point(offx, 100)
	  }
	}

	path.add( new Point( offx, view.center.y ) )

	path.strokeColor = 'rgba(255, 255, 255, .8 )'
	path.strokeWidth = 2

	this.onFrame = function( diff ) {

		var ls = path.lastSegment

		ls.point.x = this.ap.x + diff.x
		ls.point.y = this.ap.y + diff.y

		circle.fillColor.alpha =
			(Math.sin( Date.now() % 1000 / 1000 * PI2 ) + 1) * 0.1 + 0.4

		if( grad ) {
			bl.point.x = -3
			bl.point.y = view.height + 3
			br.point.x = ls.point.x
			br.point.y = view.height + 3
			path.fillColor.origin = path.bounds.topRight
			path.fillColor.destination = br.point
		}

		circle.position = ls.point

		if( grad ) {
			mp = path.segments[ 2 ]
			if( mp && mp.point.x < -1000 ) {
				path.removeSegment( 2 )
			}
		} else {
			mp = path.segments[ 0 ]
			if( mp && mp.point.x < -1000 ) {
				path.removeSegment( 0 )
			}
		}

	}

}

function newPoint() {

	var sinx, s

	x = rand() * np.x.rnd + np.x.con
	y = rand() * np.y.rng - np.y.bal
	x = 10


	if( y > 0 ) {
		y = Math.pow( y, np.y.pow )
	} else {
		y = Math.pow( Math.abs(y), np.y.pow ) * -1
	}

	sinx = curFrame % np.y.sinDuration / np.y.sinDuration

	s = Math.sin( sinx * PI2 ) * np.y.sinWeight
	y += s

	return new Point( x, y )

}

},{"./bars":2}],6:[function(require,module,exports){
require( './const' )
require( './sidenav' )

var floor = Math.floor
var win = $( window )
var bod = $( 'body' )
var top = $( '#top' )
var nav = $( 'nav.main' )

require( './anim' )

initNav()
win.on( 'scroll', onScroll )
onScroll()

function initNav() {
	var links = $( 'nav.main ul li a' )

	links.each(function(index, el) {

		el = $(el)
		var h = el.attr( 'href' )
		console.log( h )

		if( h[0] === '#' ) {
			var a = $( el.attr( 'href' ) )

			win.on( 'scroll', function() {
				var ot = a.offset().top - 120
				var st = win.scrollTop()
				if( st > ot ) {
					links.each( function(i, e) { $(e).removeClass( 'active' ) })
					el.addClass( 'active' )
				}
			})
		}
	})
}

$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
		closeNav()
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 118
      }, 400, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

function onScroll() {
	var st = win.scrollTop()
	var navTgt = top.height() - 100
	var navHeight = nav.height() + 5
	var perc = st / navTgt
	if( perc > 1 ) perc = 1
	nav.css({ top: perc * navHeight - navHeight })
}

},{"./anim":1,"./const":3,"./sidenav":4}]},{},[6])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy5udm0vdmVyc2lvbnMvbm9kZS92Ny4xMC4wL2xpYi9ub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwic3JjL21haW4vYW5pbS5qcyIsInNyYy9tYWluL2JhcnMuanMiLCJzcmMvbWFpbi9jb25zdC5qcyIsInNyYy9tYWluL3NpZGVuYXYuanMiLCJzcmMvbWFpbi9zdG9ja0xpbmVzLmpzIiwic3JjL21haW4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInZhciB0b3AgPSAkKCAnI3RvcCcgKVxudmFyIGNhbiA9ICQoICc8Y2FudmFzPicgKVxudmFyIHdpbiA9ICQod2luZG93KVxudmFyIHN0b2NrTGluZXMgPSByZXF1aXJlKCAnLi9zdG9ja0xpbmVzJyApXG52YXIgbDEsIGwyLCBsMywgd29ybGQxLCB3b3JsZDJcblxuJChpbml0KVxuXG5mdW5jdGlvbiBpbml0KCkge1xuXHRjYW4uY3NzKHtcblx0XHRiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsMCwwLDAuMjUpJyxcblx0XHQvL3RyYW5zZm9ybTogJ3JvdGF0ZVkoNDVkZWcpJ1xuXHR9KVxuXG5cdHRvcC5hcHBlbmQoIGNhbiApXG5cblx0cGFwZXIuaW5zdGFsbCggd2luZG93IClcblx0cGFwZXIuc2V0dXAoIGNhblswXSApXG5cblx0bDEgPSBuZXcgTGF5ZXIoKVxuXHRsMiA9IG5ldyBMYXllcigpXG5cdGwzID0gbmV3IExheWVyKClcblxuXHRsMS5hY3RpdmF0ZSgpXG5cdHByb2plY3QuaW1wb3J0U1ZHKCAnYW5pbS93b3JsZC5zdmcnLCB7b25Mb2FkOiBmdW5jdGlvbiggc3ZnICl7XG5cdFx0d29ybGQxID0gc3ZnXG5cdFx0d29ybGQxLm9wYWNpdHkgPSAuM1xuXHRcdHdvcmxkMiA9IHdvcmxkMS5jbG9uZSgpXG5cdFx0d29ybGQxLmZpbGxDb2xvciA9ICdyZWQnXG5cdFx0d29ybGQyLmZpbGxDb2xvciA9ICdyZWQnXG5cdFx0cmVzaXplKClcblx0XHRzdG9ja0xpbmVzLmNyZWF0ZSggbDMsIGwyKVxuXHRcdHBhcGVyLnZpZXcub25GcmFtZSA9IG9uRnJhbWVcblx0XHR3aW4ub24oICdyZXNpemUnLCByZXNpemUgKVxuXHRcdHJlc2l6ZSgpXG5cdH19IClcblxufVxuXG5mdW5jdGlvbiByZXNpemUoKSB7XG5cblx0dmFyIHcgPSB3aW4ud2lkdGgoKVxuXHR2YXIgaCA9IHdpbi5oZWlnaHQoKVxuXHR2YXIgd3csIHdoXG5cdGNhbi5hdHRyKCB7IHdpZHRoOiB3LCBoZWlnaHQ6IGggfSApXG5cdGNhbi5jc3MoIHsgd2lkdGg6IHcsIGhlaWdodDogdyB9IClcblx0Y2FuLmNzcyggeyBiYWNrZ3JvdW5kQ29sb3I6ICd0cmFuc3BhcmVudCcgfSApXG5cdHRvcC5hdHRyKCB7IHdpZHRoOiB3LCBoZWlnaHQ6IHcgfSApXG5cdHZpZXcud2lkdGggPSB3XG5cdHZpZXcuaGVpZ2h0ID0gaFxuXHRpZiggd29ybGQxICkge1xuXHRcdHZhciB3dyA9IHZpZXcud2lkdGggLyB3b3JsZDEuYm91bmRzLndpZHRoXG5cdFx0dmFyIHdoID0gdmlldy5oZWlnaHQgLyB3b3JsZDEuYm91bmRzLmhlaWdodCAqIC43XG5cdFx0d29ybGQxLnNjYWxpbmcgPSB3b3JsZDIuc2NhbGluZyA9IHd3ID4gd2ggPyB3dyA6IHdoXG5cdFx0d29ybGQxLnBvc2l0aW9uID0gdmlldy5jZW50ZXJcblx0XHR3b3JsZDIucG9zaXRpb24gPSB3b3JsZDEucG9zaXRpb24uY2xvbmUoKVxuXHRcdHdvcmxkMi5wb3NpdGlvbi54ICs9IHdvcmxkMi5ib3VuZHMud2lkdGhcblx0fVxuXG59XG5cbmZ1bmN0aW9uIG9uRnJhbWUoKSB7XG5cblx0dmFyIHcgPSB3aW4ud2lkdGgoKVxuXHR2YXIgaCA9IHdpbi5oZWlnaHQoKVxuXHR2YXIgc2wsIGxlbiA9IHN0b2NrTGluZXMubGVuZ3RoXG5cblx0Y2FuLmF0dHIoIHsgd2lkdGg6IHcsIGhlaWdodDogaCB9IClcblx0Y2FuLmNzcyggeyB3aWR0aDogdywgaGVpZ2h0OiBoIH0gKVxuXHR2aWV3LnNpemUud2lkdGggPSB3XG5cdHZpZXcuc2l6ZS5oZWlnaHQgPSBoXG5cblx0c3RvY2tMaW5lcy5vbkZyYW1lKClcblxuXHRpZiggd29ybGQxICkge1xuXG5cdFx0d29ybGQxLnBvc2l0aW9uLnggLT0gLjVcblx0XHR3b3JsZDIucG9zaXRpb24ueCAtPSAuNVxuXHRcdHdvcmxkMS5wb3NpdGlvbi55ID0gd29ybGQyLnBvc2l0aW9uLnkgPSB2aWV3LmNlbnRlci55XG5cblx0XHRpZiggd29ybGQxLnBvc2l0aW9uLnggPCB2aWV3LmNlbnRlci54IC0gd29ybGQxLmJvdW5kcy53aWR0aCApIHtcblx0XHRcdHdvcmxkMS5wb3NpdGlvbi54ID0gdmlldy5jZW50ZXIueCArIHdvcmxkMS5ib3VuZHMud2lkdGhcblx0XHR9XG5cdFx0aWYoIHdvcmxkMi5wb3NpdGlvbi54IDwgdmlldy5jZW50ZXIueCAtIHdvcmxkMi5ib3VuZHMud2lkdGggKSB7XG5cdFx0XHR3b3JsZDIucG9zaXRpb24ueCA9IHZpZXcuY2VudGVyLnggKyB3b3JsZDIuYm91bmRzLndpZHRoXG5cdFx0fVxuXHR9XG5cbn1cbiIsInZhciBiYXJzID0gT2JqZWN0LmNyZWF0ZSggbnVsbCApXG52YXIgbGF5ZXJcblxuYmFycy5jcmVhdGUgPSBmdW5jdGlvbiggX2xheWVyICkge1xuXHRsYXllciA9IF9sYXllclxuXHRjcmVhdGVCYXIoIDAgKVxufVxuXG5iYXJzLmNyZWF0ZUJhciA9IGNyZWF0ZUJhclxuXG5tb2R1bGUuZXhwb3J0cyA9IGJhcnNcblxuZnVuY3Rpb24gY3JlYXRlQmFyKCB4ICkge1xuXHR2YXIgZiA9IDBcblx0dmFyIHBhdGgsIGggPSB2aWV3LmhlaWdodCAqIDAuMjUgKyByYW5kKCkgKiB2aWV3LmhlaWdodCAqIDAuMjVcblx0bGF5ZXIuYWN0aXZhdGUoKVxuXHRwYXRoID0gbmV3IFBhdGguUmVjdGFuZ2xlKCB4ICsgMCwgdmlldy5oZWlnaHQsIDUwLCAwIClcblx0cGF0aC5maWxsQ29sb3IgPSAncmdiYSggMjUwLCAwLCAwLCAwLjUgKSdcblx0cGF0aC5zdHJva2VDb2xvciA9ICdyZ2JhKCAyNTUsIDAsIDAsIC44ICknXG5cdHNldFRpbWVvdXQoIGZhZGVpbiwgMCApXG5cblx0ZnVuY3Rpb24gZmFkZWluKCkge1xuXHRcdHZhciBzMiA9IHBhdGguc2VnbWVudHNbMV1cblx0XHR2YXIgczMgPSBwYXRoLnNlZ21lbnRzWzJdXG5cdFx0dmFyIHM0ID0gcGF0aC5zZWdtZW50c1szXVxuXHRcdGYgKz0gMC4wNVxuXHRcdHMzLnBvaW50LnkgPSBzMi5wb2ludC55ID0gczQucG9pbnQueSAtIGYgKiBoXG5cblx0XHRpZiggZiA8IDEgKSBzZXRUaW1lb3V0KCBmYWRlaW4sIDUwIClcblx0fVxufVxuIiwid2luZG93LlBJID0gTWF0aC5QSVxud2luZG93LlBJMiA9IFBJICogMlxud2luZG93LnJhbmQgPSBNYXRoLnJhbmRvbVxud2luZG93LkNPTlNUID0ge1xuXHRuZXdQb2ludDoge1xuXHRcdHJhdGU6IDUsXG5cdFx0eDoge1xuXHRcdFx0cm5kOiAwLFxuXHRcdFx0Y29uOiAxMFxuXHRcdH0sXG5cdFx0eToge1xuXHRcdFx0cm5nOiAxMDAwMCxcblx0XHRcdGJhbDogNjIwMCxcblx0XHRcdHBvdzogLjMsXG5cdFx0XHRzaW5EdXJhdGlvbjogMjUwLFxuXHRcdFx0c2luV2VpZ2h0OiA1XG5cdFx0fVxuXHR9LFxuXHRtZWRpYW5MZW5ndGg6IDUsXG5cdHN0b2NrTGluZToge1xuXHRcdGdyYWRpZW50U3RvcHM6IFtcInJnYmEoIDI1NSwgMjU1LCAyNTUsIC4yNSApXCIsIFwicmdiYSggMjU1LCAyNTUsIDI1NSwgLjc1KVwiLCBcInJnYmEoIDI1NSwgMjU1LCAyNTUsIC41KVwiLCBcInJnYmEoIDI1NSwgMCwwLCAwLjI1KVwiLCBcInJnYmEoIDAsIDAsIDAsIDApXCJdLFxuXHRcdGdyYWRpZW50U3RvcHMyOiBbXCJyZ2JhKCAwLCAwLCAyNTUsIC45IClcIiwgXCJyZ2JhKCAwLCAwLCAyNTUsIC44NSlcIiwgXCJyZ2JhKCAwLCAwLCAyNTUsIC44KVwiLCBcInJnYmEoIDI1NSwgMCwwLCAwLjI1KVwiLCBcInJnYmEoIDAsIDAsIDAsIDApXCJdXG5cdH1cbn1cbiIsInZhciBib2R5ID0gJCggJ2JvZHknIClcblxud2luZG93Lm9wZW5OYXYgPSBmdW5jdGlvbiBvcGVuTmF2KCkge1xuICAgIGJvZHkuYWRkQ2xhc3MoICduYXZPcGVuJyApXG59XG5cbndpbmRvdy5jbG9zZU5hdiA9IGZ1bmN0aW9uIGNsb3NlTmF2KCkge1xuICAgIGJvZHkucmVtb3ZlQ2xhc3MoICduYXZPcGVuJyApXG59XG4iLCJ2YXIgc3RvY2tMaW5lcyA9IFtdXG52YXIgbnAgPSBDT05TVC5uZXdQb2ludFxudmFyIGN1ckZyYW1lID0gMFxudmFyIG1jb3VudCA9IDBcbnZhciBzbDEsIHNsMiwgc2wzLCBuZXh0UG9pbnRcbnZhciBuZXh0TWVkaWFuLCBuZXh0TWVkaWFuMiwgbGF5ZXIsIGJhcnNMYXllclxudmFyIGJhcnMsIHNob3VsZENyZWF0ZUJhclxuXG5zdG9ja0xpbmVzLmNyZWF0ZSA9IGZ1bmN0aW9uKCBfbGF5ZXIsIF9iYXJzTGF5ZXIgKSB7XG5cblx0bGF5ZXIgPSBfbGF5ZXIsIGJhcnNMYXllciA9IF9iYXJzTGF5ZXJcblx0YmFycyA9IHJlcXVpcmUoICcuL2JhcnMnIClcblx0YmFycy5jcmVhdGUoIGJhcnNMYXllciApXG5cdGxheWVyLmFjdGl2YXRlKClcblx0bGF5ZXIuYWRkQ2hpbGQoIGJhcnNMYXllciApXG5cdHNsMSA9IG5ldyBTdG9ja0xpbmUoIHRydWUgKVxuXHRzbDIgPSBuZXcgU3RvY2tMaW5lKCBmYWxzZSwgQ09OU1Quc3RvY2tMaW5lLmdyYWRpZW50U3RvcHMyIClcblx0c2wzID0gbmV3IFN0b2NrTGluZSgpXG5cblx0c2wyLmNpcmNsZS5maWxsQ29sb3IgPSAncmdiYSggMCwgMTAwLCAyNTUsIDEgKSdcblx0c2wyLnBhdGguc3Ryb2tlQ29sb3IgPSAncmdiYSggMCwgMTAwLCAyNTUsIDEgKSdcblxuXHRzbDMuY2lyY2xlLmZpbGxDb2xvciA9ICdyZ2JhKCAyMDAsIDAsIDAsIDEgKSdcblx0c2wzLnBhdGguc3Ryb2tlQ29sb3IgPSAncmdiYSggMjAwLCAwLCAwLCAxICknXG5cblx0Y3JlYXRlTmV3UG9pbnQoKVxuXG59XG5cbnN0b2NrTGluZXMub25GcmFtZSA9IGZ1bmN0aW9uKCkge1xuXG5cdHZhciBtcmF0ZSA9IChucC5yYXRlICogQ09OU1QubWVkaWFuTGVuZ3RoKVxuXHR2YXIgbXJhdGUyID0gbXJhdGUgKiBDT05TVC5tZWRpYW5MZW5ndGhcblx0dmFyIHBlcmMgPSAoY3VyRnJhbWUrKyAlIG5wLnJhdGUpIC8gbnAucmF0ZVxuXHR2YXIgcGVyYzIgPSBjdXJGcmFtZSAlIG1yYXRlIC8gbXJhdGVcblx0dmFyIHBlcmMzID0gY3VyRnJhbWUgJSBtcmF0ZTIgLyBtcmF0ZTJcblx0dmFyIGRpZmYgPSBuZXh0UG9pbnQubXVsdGlwbHkoIHBlcmMgKVxuXG5cdHZhciBkaWZmMiwgZGlmZjNcblxuXHRpZiggcGVyYyA9PT0gMCApIGNyZWF0ZU5ld1BvaW50KClcblxuXHRzbDEub25GcmFtZSggZGlmZiApXG5cblx0aWYoIHBlcmMyID09PSAwICkgY3JlYXRlTmV3TWVkaWFuKClcblx0aWYoIHBlcmMzID09PSAwICkgY3JlYXRlTmV3TWVkaWFuMigpXG5cblx0aWYoIG5leHRNZWRpYW4gKSB7XG5cdFx0ZGlmZjIgPSBuZXh0TWVkaWFuLm11bHRpcGx5KCBwZXJjMiApXG5cdFx0c2wyLm9uRnJhbWUoIGRpZmYyIClcblx0fVxuXHRpZiggbmV4dE1lZGlhbjIgKSB7XG5cdFx0ZGlmZjMgPSBuZXh0TWVkaWFuMi5tdWx0aXBseSggcGVyYzMgKVxuXHRcdHNsMy5vbkZyYW1lKCBkaWZmMyApXG5cdH1cblxuXHRpZiggbGF5ZXIuYm91bmRzLndpZHRoID4gdmlldy53aWR0aCAqIDAuOTggKSB7XG5cdFx0bGF5ZXIucG9zaXRpb24ueCA9IC0gKGxheWVyLmJvdW5kcy53aWR0aCAvIDIgKSArIHZpZXcud2lkdGggKiAuOThcblx0fVxuXG5cdGxheWVyLnBvc2l0aW9uLnkgKz0gLjEgKiAoKCB2aWV3LmhlaWdodCAqMC40ICsgbGF5ZXIuYm91bmRzLmhlaWdodCAqIC41ICkgLSBsYXllci5wb3NpdGlvbi55IClcblxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHN0b2NrTGluZXNcblxuZnVuY3Rpb24gY3JlYXRlTmV3UG9pbnQoKSB7XG5cdHZhciBjbG9uZVxuXHRzbDEuYXAgPSBzbDEucGF0aC5sYXN0U2VnbWVudC5wb2ludFxuXHRuZXh0UG9pbnQgPSBuZXdQb2ludCgpXG5cdGNsb25lID0gc2wxLmFwLmNsb25lKClcblx0c2wxLnBhdGguYWRkKCBjbG9uZSApXG59XG5cbmZ1bmN0aW9uIGNyZWF0ZU5ld01lZGlhbigpIHtcblx0dmFyIE1MID0gQ09OU1QubWVkaWFuTGVuZ3RoXG5cdHZhciBpLCB4MSA9IHNsMS5wYXRoLnNlZ21lbnRzLmxlbmd0aCAtIE1MLCB4MiA9IHgxICsgTUxcblx0dmFyIHN1bSA9IDBcblx0dmFyIGNsb25lXG5cblx0c2wyLmFwID0gc2wyLnBhdGgubGFzdFNlZ21lbnQucG9pbnRcblxuXHRmb3IoIGkgPSB4MTsgaSA8IHgyOyArK2kgKSB7XG5cblx0XHRzdW0gKz0gc2wxLnBhdGguc2VnbWVudHNbIGkgXS5wb2ludC55XG5cdH1cblx0bmV4dE1lZGlhbiA9IG5ldyBQb2ludChcblx0XHRzbDEucGF0aC5zZWdtZW50c1sgeDEgXS5wb2ludC54IC0gc2wyLmFwLnggKyAyMCxcblx0XHRzdW0gLyBNTCAtIHNsMi5hcC55XG5cdClcblx0Y2xvbmUgPSBzbDIuYXAuY2xvbmUoKVxuXHRzbDIucGF0aC5hZGQoIGNsb25lIClcblx0c2hvdWxkQ3JlYXRlQmFyID0gIXNob3VsZENyZWF0ZUJhclxuXHRpZiggeDIgPiAxMCAmJiBzaG91bGRDcmVhdGVCYXIgKSBiYXJzLmNyZWF0ZUJhciggY2xvbmUueCApXG59XG5cbmZ1bmN0aW9uIGNyZWF0ZU5ld01lZGlhbjIoKSB7XG5cdHZhciBNTCA9IENPTlNULm1lZGlhbkxlbmd0aFxuXHR2YXIgaSwgeDEgPSBzbDIucGF0aC5zZWdtZW50cy5sZW5ndGggLSBNTCwgeDIgPSB4MSArIE1MXG5cdHZhciBzdW0gPSAwXG5cdHZhciBjbG9uZVxuXG5cdHNsMy5hcCA9IHNsMy5wYXRoLmxhc3RTZWdtZW50LnBvaW50XG5cblx0Zm9yKCBpID0geDE7IGkgPCB4MjsgKytpICkge1xuXG5cdFx0c3VtICs9IHNsMi5wYXRoLnNlZ21lbnRzWyBpIF0ucG9pbnQueVxuXG5cdH1cblxuXHRuZXh0TWVkaWFuMiA9IG5ldyBQb2ludChcblx0XHRzbDIucGF0aC5zZWdtZW50c1sgeDEgXS5wb2ludC54IC0gc2wzLmFwLnggKyAyNzAsXG5cdFx0c3VtIC8gTUwgLSBzbDMuYXAueVxuXHQpXG5cdGNsb25lID0gc2wzLmFwLmNsb25lKClcblx0c2wzLnBhdGguYWRkKCBjbG9uZSApXG5cbn1cblxuZnVuY3Rpb24gU3RvY2tMaW5lKCBncmFkLCBzdG9wcyApIHtcblx0dmFyIG9mZnggPSAtMjVcblx0dmFyIHBhdGggPSB0aGlzLnBhdGggPSBuZXcgUGF0aCgpXG5cdHZhciBjaXJjbGUgPSB0aGlzLmNpcmNsZSA9IG5ldyBQYXRoLkNpcmNsZSggbmV3IFBvaW50KCBvZmZ4LCB2aWV3LmNlbnRlci55ICksIDQgKVxuXHR2YXIgYnIsIGJsLCBhcFxuXG5cdGNpcmNsZS5maWxsQ29sb3IgPSAnd2hpdGUnXG5cdHRoaXMucGVyYyA9IDBcblx0c3RvY2tMaW5lcy5wdXNoKCB0aGlzIClcblxuXHRpZiggZ3JhZCApIHtcblx0XHRiciA9IHBhdGguYWRkKCBuZXcgUG9pbnQoIG9mZngsIHZpZXcuY2VudGVyLnkpIClcblx0XHRibCA9IHBhdGguYWRkKCBuZXcgUG9pbnQoIG9mZngsIHZpZXcuY2VudGVyLnkpIClcblx0XHRwYXRoLmZpbGxDb2xvciA9IHtcblx0ICAgIGdyYWRpZW50OiB7XG5cdCAgICAgIHN0b3BzOiBzdG9wcyB8fCBDT05TVC5zdG9ja0xpbmUuZ3JhZGllbnRTdG9wc1xuXHQgICAgfSxcblx0ICAgIG9yaWdpbjogbmV3IFBvaW50KG9mZngsIDApLFxuXHQgICAgZGVzdGluYXRpb246IG5ldyBQb2ludChvZmZ4LCAxMDApXG5cdCAgfVxuXHR9XG5cblx0cGF0aC5hZGQoIG5ldyBQb2ludCggb2ZmeCwgdmlldy5jZW50ZXIueSApIClcblxuXHRwYXRoLnN0cm9rZUNvbG9yID0gJ3JnYmEoMjU1LCAyNTUsIDI1NSwgLjggKSdcblx0cGF0aC5zdHJva2VXaWR0aCA9IDJcblxuXHR0aGlzLm9uRnJhbWUgPSBmdW5jdGlvbiggZGlmZiApIHtcblxuXHRcdHZhciBscyA9IHBhdGgubGFzdFNlZ21lbnRcblxuXHRcdGxzLnBvaW50LnggPSB0aGlzLmFwLnggKyBkaWZmLnhcblx0XHRscy5wb2ludC55ID0gdGhpcy5hcC55ICsgZGlmZi55XG5cblx0XHRjaXJjbGUuZmlsbENvbG9yLmFscGhhID1cblx0XHRcdChNYXRoLnNpbiggRGF0ZS5ub3coKSAlIDEwMDAgLyAxMDAwICogUEkyICkgKyAxKSAqIDAuMSArIDAuNFxuXG5cdFx0aWYoIGdyYWQgKSB7XG5cdFx0XHRibC5wb2ludC54ID0gLTNcblx0XHRcdGJsLnBvaW50LnkgPSB2aWV3LmhlaWdodCArIDNcblx0XHRcdGJyLnBvaW50LnggPSBscy5wb2ludC54XG5cdFx0XHRici5wb2ludC55ID0gdmlldy5oZWlnaHQgKyAzXG5cdFx0XHRwYXRoLmZpbGxDb2xvci5vcmlnaW4gPSBwYXRoLmJvdW5kcy50b3BSaWdodFxuXHRcdFx0cGF0aC5maWxsQ29sb3IuZGVzdGluYXRpb24gPSBici5wb2ludFxuXHRcdH1cblxuXHRcdGNpcmNsZS5wb3NpdGlvbiA9IGxzLnBvaW50XG5cblx0XHRpZiggZ3JhZCApIHtcblx0XHRcdG1wID0gcGF0aC5zZWdtZW50c1sgMiBdXG5cdFx0XHRpZiggbXAgJiYgbXAucG9pbnQueCA8IC0xMDAwICkge1xuXHRcdFx0XHRwYXRoLnJlbW92ZVNlZ21lbnQoIDIgKVxuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cdFx0XHRtcCA9IHBhdGguc2VnbWVudHNbIDAgXVxuXHRcdFx0aWYoIG1wICYmIG1wLnBvaW50LnggPCAtMTAwMCApIHtcblx0XHRcdFx0cGF0aC5yZW1vdmVTZWdtZW50KCAwIClcblx0XHRcdH1cblx0XHR9XG5cblx0fVxuXG59XG5cbmZ1bmN0aW9uIG5ld1BvaW50KCkge1xuXG5cdHZhciBzaW54LCBzXG5cblx0eCA9IHJhbmQoKSAqIG5wLngucm5kICsgbnAueC5jb25cblx0eSA9IHJhbmQoKSAqIG5wLnkucm5nIC0gbnAueS5iYWxcblx0eCA9IDEwXG5cblxuXHRpZiggeSA+IDAgKSB7XG5cdFx0eSA9IE1hdGgucG93KCB5LCBucC55LnBvdyApXG5cdH0gZWxzZSB7XG5cdFx0eSA9IE1hdGgucG93KCBNYXRoLmFicyh5KSwgbnAueS5wb3cgKSAqIC0xXG5cdH1cblxuXHRzaW54ID0gY3VyRnJhbWUgJSBucC55LnNpbkR1cmF0aW9uIC8gbnAueS5zaW5EdXJhdGlvblxuXG5cdHMgPSBNYXRoLnNpbiggc2lueCAqIFBJMiApICogbnAueS5zaW5XZWlnaHRcblx0eSArPSBzXG5cblx0cmV0dXJuIG5ldyBQb2ludCggeCwgeSApXG5cbn1cbiIsInJlcXVpcmUoICcuL2NvbnN0JyApXG5yZXF1aXJlKCAnLi9zaWRlbmF2JyApXG5cbnZhciBmbG9vciA9IE1hdGguZmxvb3JcbnZhciB3aW4gPSAkKCB3aW5kb3cgKVxudmFyIGJvZCA9ICQoICdib2R5JyApXG52YXIgdG9wID0gJCggJyN0b3AnIClcbnZhciBuYXYgPSAkKCAnbmF2Lm1haW4nIClcblxucmVxdWlyZSggJy4vYW5pbScgKVxuXG5pbml0TmF2KClcbndpbi5vbiggJ3Njcm9sbCcsIG9uU2Nyb2xsIClcbm9uU2Nyb2xsKClcblxuZnVuY3Rpb24gaW5pdE5hdigpIHtcblx0dmFyIGxpbmtzID0gJCggJ25hdi5tYWluIHVsIGxpIGEnIClcblxuXHRsaW5rcy5lYWNoKGZ1bmN0aW9uKGluZGV4LCBlbCkge1xuXG5cdFx0ZWwgPSAkKGVsKVxuXHRcdHZhciBoID0gZWwuYXR0ciggJ2hyZWYnIClcblx0XHRjb25zb2xlLmxvZyggaCApXG5cblx0XHRpZiggaFswXSA9PT0gJyMnICkge1xuXHRcdFx0dmFyIGEgPSAkKCBlbC5hdHRyKCAnaHJlZicgKSApXG5cblx0XHRcdHdpbi5vbiggJ3Njcm9sbCcsIGZ1bmN0aW9uKCkge1xuXHRcdFx0XHR2YXIgb3QgPSBhLm9mZnNldCgpLnRvcCAtIDEyMFxuXHRcdFx0XHR2YXIgc3QgPSB3aW4uc2Nyb2xsVG9wKClcblx0XHRcdFx0aWYoIHN0ID4gb3QgKSB7XG5cdFx0XHRcdFx0bGlua3MuZWFjaCggZnVuY3Rpb24oaSwgZSkgeyAkKGUpLnJlbW92ZUNsYXNzKCAnYWN0aXZlJyApIH0pXG5cdFx0XHRcdFx0ZWwuYWRkQ2xhc3MoICdhY3RpdmUnIClcblx0XHRcdFx0fVxuXHRcdFx0fSlcblx0XHR9XG5cdH0pXG59XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gIC8vIEFkZCBzbW9vdGggc2Nyb2xsaW5nIHRvIGFsbCBsaW5rc1xuICAkKFwiYVwiKS5vbignY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xuXHRcdGNsb3NlTmF2KClcbiAgICAvLyBNYWtlIHN1cmUgdGhpcy5oYXNoIGhhcyBhIHZhbHVlIGJlZm9yZSBvdmVycmlkaW5nIGRlZmF1bHQgYmVoYXZpb3JcbiAgICBpZiAodGhpcy5oYXNoICE9PSBcIlwiKSB7XG4gICAgICAvLyBQcmV2ZW50IGRlZmF1bHQgYW5jaG9yIGNsaWNrIGJlaGF2aW9yXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAvLyBTdG9yZSBoYXNoXG4gICAgICB2YXIgaGFzaCA9IHRoaXMuaGFzaDtcblxuICAgICAgLy8gVXNpbmcgalF1ZXJ5J3MgYW5pbWF0ZSgpIG1ldGhvZCB0byBhZGQgc21vb3RoIHBhZ2Ugc2Nyb2xsXG4gICAgICAvLyBUaGUgb3B0aW9uYWwgbnVtYmVyICg4MDApIHNwZWNpZmllcyB0aGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBpdCB0YWtlcyB0byBzY3JvbGwgdG8gdGhlIHNwZWNpZmllZCBhcmVhXG4gICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgIHNjcm9sbFRvcDogJChoYXNoKS5vZmZzZXQoKS50b3AgLSAxMThcbiAgICAgIH0sIDQwMCwgZnVuY3Rpb24oKXtcblxuICAgICAgICAvLyBBZGQgaGFzaCAoIykgdG8gVVJMIHdoZW4gZG9uZSBzY3JvbGxpbmcgKGRlZmF1bHQgY2xpY2sgYmVoYXZpb3IpXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5oYXNoID0gaGFzaDtcbiAgICAgIH0pO1xuICAgIH0gLy8gRW5kIGlmXG4gIH0pO1xufSk7XG5cbmZ1bmN0aW9uIG9uU2Nyb2xsKCkge1xuXHR2YXIgc3QgPSB3aW4uc2Nyb2xsVG9wKClcblx0dmFyIG5hdlRndCA9IHRvcC5oZWlnaHQoKSAtIDEwMFxuXHR2YXIgbmF2SGVpZ2h0ID0gbmF2LmhlaWdodCgpICsgNVxuXHR2YXIgcGVyYyA9IHN0IC8gbmF2VGd0XG5cdGlmKCBwZXJjID4gMSApIHBlcmMgPSAxXG5cdG5hdi5jc3MoeyB0b3A6IHBlcmMgKiBuYXZIZWlnaHQgLSBuYXZIZWlnaHQgfSlcbn1cbiJdfQ==
